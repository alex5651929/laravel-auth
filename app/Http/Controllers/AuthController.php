<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToType(string $type)
    {
        return Socialite::driver($type)->redirect();
    }

    public function handleCallback(string $type)
    {
        $user = Socialite::driver($type)->user();
        $localUser = User::where('email', $user->getEmail())->first();

        $userData = [
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            'password' => Hash::make($user->getName().'@'.$user->getId()),
        ];

        if ('google' === $type) {
            $userData['google_id'] = $user->getId();
        } else {
            $userData['yandex_id'] = $user->getId();
        }

        if (!$localUser) {
            $localUser = User::create($userData);
        }

        Auth::login($localUser);

        Session::put('user', $localUser);

        return Redirect::to('http://localhost:3000/');
    }

    public function me(Request $request)
    {
        /** @var User $user */
        $user = $request->session()->get('user');

        if (!$user) {
            return response()->json(['error' => 'User data not found in session'], 404);
        }

        return response()->json(['email'=> $user->email]);
    }
}
