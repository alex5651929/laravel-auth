up:
	docker-compose up -d

down:
	docker-compose down

bash-app:
	 docker exec -it laravel-auth_app_1 bash

bash-app-root:
	 docker exec -u root -it laravel-auth_app_1 bash
