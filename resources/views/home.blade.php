@extends('layouts.app')

@section('title', 'This home page')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 offset-sm-3 col-md-6 offset-md-3">
                <h1>Main page</h1>
                <a href="{{ route('login') }}">
                    <button type="button">Sign In</button>
                </a>
            </div>
        </div>
    </div>
@endsection
