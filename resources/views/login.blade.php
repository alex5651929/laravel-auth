@extends('layouts.app')

@section('title', 'This login page')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 offset-sm-3 col-md-6 offset-md-3">
                <h1>This login page</h1>
                <a href="{{ route('auth.login', ['type' => 'google']) }}">
                    <button type="button">Войти через Google</button>
                </a>
                <a href="{{ route('auth.login', ['type' => 'yandex']) }}">
                    <button type="button">Войти через Yandex</button>
                </a>
            </div>
        </div>
    </div>
@endsection
