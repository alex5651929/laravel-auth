@extends('layouts.app')

@section('title', 'This my page')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 offset-sm-3 col-md-6 offset-md-3">
                <h1>Hello user {{ $user->email }}</h1>
                <a href="{{ route('logout') }}">
                    <button type="button">Logout</button>
                </a>
            </div>
        </div>
    </div>
@endsection
